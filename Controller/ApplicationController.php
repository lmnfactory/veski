<?php

namespace Lmn\App\Veski\Controller;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Account\Lib\User\UserService;
use Lmn\Core\Lib\Model\ValidationService;

class ApplicationController extends Controller {

    public function index(Request $request) {
        if ($request->cookie('JWTRefresh') || $request->cookie('RMAuth')) {
            return redirect()->route('app');
        }
        return view("lmn.app-veski::index");
    }

    public function app() {
        if (App::environment() == "development") {
            return $this->dev();
        } else if (App::environment() == "production") {
            return $this->prod();
        }
    }

    public function prod() {
        return view("angular/app");
    }

    public function dev() {
        return view("lmn.app-veski::app-dev");
    }

    public function signin(Request $request) {
        if ($request->cookie('JWTRefresh') || $request->cookie('RMAuth')) {
            return redirect()->route('app');
        }
        return view("lmn.app-veski::signin");
    }

    public function signup() {
        return view("lmn.app-veski::signup");
    }

    public function signupSuccess() {
        return view("lmn.app-veski::signup_success");
    }

    public function resetPassword() {
        return view("lmn.app-veski::reset_password");
    }

    public function resetPasswordSuccess() {
        return view("lmn.app-veski::reset_password_success");
    }

    public function emailValidation(Request $request, UserService $userService, ValidationService $validationService) {
        $data = [
            'token' => $request->input('token'),
            'email' => $request->input('email'),
        ];
        $viewData = [
            'success' => true
        ];

        // valiadte user input (form)
        if (!$validationService->validate($data, 'form.validate')) {
            $viewData['success'] = false;
        }
        // do email validation of a user
        $user = $userService->validate($data['email'], $data['token']);
        if ($user == null) {
            $viewData['success'] = false;
        }
        
        return view("lmn.app-veski::email_validation", $viewData);
    }
}
