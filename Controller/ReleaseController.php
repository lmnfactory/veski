<?php

namespace Lmn\App\Veski\Controller;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReleaseController extends Controller {

    public function index() {
        return view("lmn.app-veski::release_log.release_20170830");
    }
}
