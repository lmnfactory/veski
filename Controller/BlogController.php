<?php

namespace Lmn\App\Veski\Controller;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Account\Lib\User\UserService;
use Lmn\Core\Lib\Model\ValidationService;

class BlogController extends Controller {

    public function index(Request $request) {
        return view("lmn.app-veski::blog.index", [
            'list' => [
                [
                    'title' => 'Trello pre začínajúci projekt',
                    'url' => 'blog/trello',
                    'thumbnail' => 'storage/blog/trello/thumbnail.png',
                    'text' => 'Trello je krásny nástroj na organizovanie projektu. Nedostatkom je, že ti nikto neukáže ako si ho nastaviť. Existujú preset, ale nikdy som k ním nenašiel vysvetlenie prečo je to takto dobré alebo návod ako by sa mal používať. Preto sa pokusim v tomto článku ukázť, do detailu, preset na vývoj webovej aplikácie v malej skupine ľudí.'
                ]
            ]
        ]);
    }

    public function view(Request $request) {
        return view("lmn.app-veski::blog.article.trello", [
            'imagePath' => "storage/blog/trello/"
        ]);
    }
}
