<?php

namespace Lmn\App\Veski\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Account\Lib\Auth\CurrentUser;

use Lmn\Subject\Repository\SubjectRepository;
use Lmn\Subject\Repository\SubjectUserRepository;
use Lmn\Subject\Lib\Subject\SubjectUserService;
use Lmn\Thread\Lib\Thread\ThreadService;

use Lmn\App\Veski\Lib\Colorpalette\ColorpaletteService;

class SubjectUserController extends Controller {

    public function join(Request $request, ResponseService $responseService, ValidationService $validationService, SubjectuserRepository $subjectuserRepo, SubjectRepository $subjectRepo, CurrentUser $currentUser, ColorpaletteService $colorpaletteService) {
        $data = $request->json()->all();
        $data['user_id'] = $currentUser->getId();

        $validationService->systemValidate($data, 'subject.join');

        if (!isset($data['usersettings'])) {
            $data['usersettings'] = [];
        }
        if (!isset($data['usersettings']['colorpalette_id'])) {
            $subjectUser = $subjectRepo->clear()
                ->criteria('subject.default')
                ->criteria('subject.with.all')
                ->criteria('subjectuser.by.user', ['userId' => $currentUser->getId()])
                ->all();
            
            $colors = $colorpaletteService->all();

            if (count($subjectUser) > count($colors)) {
                $color = $colorpaletteService->getRandomColor();
            } else {
                $notIn = [];
                foreach ($subjectUser as $subject) {
                    $notIn[] = $subject->usersettings->settings->colorpalette_id;
                }
                $color = $colorpaletteService->getRandomColor($notIn);
            }

            $data['usersettings']['colorpalette_id'] = $color->id;
        }

        $subjectUser = $subjectuserRepo->clear()
            ->create($data);

        $subjectUser = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('subjectuser.by.user', ['userId' => $currentUser->getId()])
            ->all();

        return $responseService->response($subjectUser);
    }

    public function leave(Request $request, ResponseService $responseService, ValidationService $validationService, SubjectRepository $subjectRepo, SubjectuserRepository $subjectuserRepo, CurrentUser $currentUser) {
        $data = $request->json()->all();
        $userId = $currentUser->getId();

        $validationService->systemValidate($data, 'subject.leave');

        $subjectUser = $subjectuserRepo->clear()
            ->criteria('subjectuser.unique', ['subjectId' => $data['subject_id'], 'userId' => $userId])
            ->delete();

        $subjectUser = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('subjectuser.by.user', ['userId' => $currentUser->getId()])
            ->all();

        return $responseService->response($subjectUser);
    }
}
