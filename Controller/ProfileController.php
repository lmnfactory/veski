<?php

namespace Lmn\App\Veski\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Exception\SystemException;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Repository\UserRepository;
use Lmn\Account\Repository\UserPublicRepository;
use Lmn\Account\Exception\AccessDeniedException;

class ProfileController extends Controller {

    private function isMyAccount($currentUser, $publicId) {
        return ($currentUser->public_id == $publicId);
    }

    public function publicProfile(Request $request, AuthService $authService, ResponseService $responseService, UserRepository $userRepo) {
        $publicId = $request->json('account');

        $profile = $userRepo->clear()
            ->criteria('user.by.publicId', ['publicId' => $publicId])
            ->permission('user')
            ->get();

        return $responseService->response($profile);
    }

    public function privateProfile(Request $request, AuthService $authService, ResponseService $responseService, UserRepository $userRepo) {
        $publicId = $request->json('account');
        $user = $authService->getCurrentUser();

        if (!$this->isMyAccount($user, $publicId)) {
            throw new AccessDeniedException("Not enough permission to see this account.");
        }

        $profile = $userRepo->clear()
            ->criteria('user.by.publicId', ['publicId' => $publicId])
            ->permission('owner')
            ->get();

        return $responseService->response($profile);
    }

    public function update(Request $request, AuthService $authService, ResponseService $responseService, UserRepository $userRepo) {
        $publicId = $request->json('account');
        $data = $request->json('data');
        $user = $authService->getCurrentUser();

        if (!$this->isMyAccount($user, $publicId)) {
            throw new AccessDeniedException("Not enough permission to edit this account.");
        }

        $profile = $userRepo->clear()
            ->criteria('user.by.publicId', ['publicId' => $publicId])
            ->update($data);

        return $responseService->response($profile);
    }
}
