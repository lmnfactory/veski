<?php

namespace Lmn\App\Veski\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Database\Model\User;

use Lmn\Notification\Lib\Notification\NotificationService;
use Lmn\Notification\Lib\Notification\NotificationMessage;

class NotificationController extends Controller {

    public function notification(Request $request, NotificationService $notificationService, AuthService $authService, ResponseService $responseService) {
        $user = $authService->getCurrentUser();
        $notifyUsers = User::where('id', '!=', $user->id)->get();

        foreach ($notifyUsers as $u) {
            $message = new NotificationMessage(['body' => "to mas spravu"], $u->id);
            $notificationService->notify($message);
        }

        return $responseService->response("All send");
    }
}
