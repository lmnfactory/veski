<?php

namespace Lmn\App\Veski\Build\Test;

use TestCase;

class NotFoundExceptionTest extends TestCase {

    public function test404() {
        $response = $this->call('GET', '/nieco-odvedci');

        $this->assertEquals(404, $response->status());
    }
}
