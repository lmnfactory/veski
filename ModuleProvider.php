<?php

namespace Lmn\App\Veski;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Illuminate\Support\Facades\Route;

use Lmn\Core\Lib\Database\Seeder\SeederService;
use Lmn\Core\Lib\Cache\CacheService;

use Lmn\Account\Middleware\SigninRequiredMiddleware;
use Lmn\Account\Repository\UserRepository;

use Lmn\Notification\Lib\Notification\NotificationService;
use Lmn\Notification\Lib\Notification\PushServerHandler;

use Lmn\Websocket\Server\Lib\Server\WebsocketServer;
use Lmn\Websocket\Server\Lib\Server\Pusher;
use Lmn\Websocket\Server\Lib\Server\BasicConnectionHandler;
use Lmn\Websocket\Server\Lib\Server\AuthConnectionHandler;

use Lmn\Sharedcalendar\Repository\CalendareventSubjectRepository;
use Lmn\Thread\Repository\ThreadRepository;
use Lmn\Subject\Repository\SubjectUserForUserRepository;
use Lmn\Subject\Repository\SubjectUserRepository;
use Lmn\Subject\Repository\SubjectRepository;

use Lmn\Core\Lib\Exception\ExceptionService;
use Lmn\App\Veski\Lib\Exception\NotFoundExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Lmn\App\Veski\Lib\Colorpalette\ColorpaletteService;
use Lmn\App\Veski\Repository\UsersettingsRepository;
use Lmn\App\Veski\Repository\SubjectusersettingsRepository;
use Lmn\App\Veski\Repository\Extension\UsersettingsRepositoryExtension;
use Lmn\App\Veski\Repository\Extension\SubjectusersettingsRepositoryExtension;
use Lmn\App\Veski\Repository\Criteria\UsersettingsPermissionCriteria;
use Lmn\App\Veski\Repository\Criteria\UsersettingsWithAllCriteria;
use Lmn\App\Veski\Repository\Criteria\SubjectExtensionCountCriteria;
use Lmn\App\Veski\Repository\Criteria\SubjectUserCountUnreadThreadCriteria;
use Lmn\App\Veski\Repository\Listener\SubjectusersettingsListener;
use Lmn\App\Veski\Repository\Listener\SubjectCountRepositoryListener;
use Lmn\App\Veski\Repository\Listener\UsersettingsRepositoryListener;
use Lmn\App\Veski\Database\Seed\ColorpaletteSeeder;
use Lmn\App\Veski\Database\Seed\SubjectusersettingsSeeder;
use Lmn\App\Veski\Database\Seed\UsersettingsSeeder;
use Lmn\App\Veski\Lib\Cache\ColorpaletteCache;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        $app->singleton(UsersettingsRepository::class, UsersettingsRepository::class);
        $app->singleton(UsersettingsRepositoryExtension::class, UsersettingsRepositoryExtension::class);
        $app->singleton(SubjectusersettingsRepository::class, SubjectusersettingsRepository::class);
        $app->singleton(SubjectusersettingsRepositoryExtension::class, SubjectusersettingsRepositoryExtension::class);
        $app->singleton(ColorpaletteRepository::class, ColorpaletteRepository::class);
        $app->singleton(ColorpaletteService::class, ColorpaletteService::class);
        $app->singleton('SubjectCalendarevent.Repository.Listener', function($app) {
            return new SubjectCountRepositoryListener('count_calendarevents', $app->make(CalendareventSubjectRepository::class));
        });
        $app->singleton('SubjectThread.Repository.Listener', function($app) {
            return new SubjectCountRepositoryListener('count_threads', $app->make(ThreadRepository::class));
        });
        $app->singleton('SubjectUser.Repository.Listener', function($app) {
            return new SubjectCountRepositoryListener('count_members', $app->make(SubjectUserRepository::class));
        });
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        $notificationService = \App::make(NotificationService::class);
        $notificationService->setMethod(new PushServerHandler('tcp://127.0.0.1:5509'));

        $websocketServer = \App::make(WebsocketServer::class);
        $pusher = new Pusher();
        $pusher->setConnectionHandler(new AuthConnectionHandler());
        $websocketServer->setPusherService($pusher);
        $websocketServer->setSocketAddr("tcp://127.0.0.1:5509");
        $websocketServer->setWebsocket('0.0.0.0', '5510');

        /** @var UserRepository */
        $userRepository = $app->make(UserRepository::class);
        $userRepository->on('*', new UsersettingsRepositoryListener($app->make(UsersettingsRepository::class)));
        /** @var RepositoryConfig */
        $subjectRepository = $app->make(SubjectRepository::class);
        $subjectRepository->on(['get', 'list'], $app->make('SubjectCalendarevent.Repository.Listener'));
        $subjectRepository->on(['get', 'list'], $app->make('SubjectThread.Repository.Listener'));
        $subjectRepository->on(['get', 'list'], $app->make('SubjectUser.Repository.Listener'));
        $subjectRepository->on(['get', 'list'], new SubjectusersettingsListener($app->make(SubjectusersettingsRepository::class)));

        /** @var SubjectUserForUserRepository */
        $subejctuserRepository = $app->make(SubjectUserRepository::class);
        $subejctuserRepository->on(['create'], new SubjectusersettingsListener($app->make(SubjectusersettingsRepository::class)));

        /** @var CriteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('usersettings.permission', UsersettingsPermissionCriteria::class);
        $criteriaService->add('usersettings.with.all', UsersettingsWithAllCriteria::class);
        $criteriaService->add('subject.ext.count', SubjectExtensionCountCriteria::class);
        $criteriaService->add('subjectuser.count.unreadthread', SubjectUserCountUnreadThreadCriteria::class);

        /** @var SeederService */
        $seederService = $app->make(SeederService::class);
        $seederService->addSeeder(ColorpaletteSeeder::class);
        $seederService->addSeeder(SubjectusersettingsSeeder::class);
        $seederService->addSeeder(UsersettingsSeeder::class);

        /** @var CacheService */
        $cacheService = $app->make(CacheService::class);
        $cacheService->add('colorpalette', ColorpaletteCache::class);

        /** @var ExceptioNService */
        $exceptionService = $provider->getApp()->make(ExceptionService::class);
        $exceptionService->add(NotFoundHttpException::class, new NotFoundExceptionHandler());
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\App\\Veski\\Controller'], function() {
            Route::any('/', 'ApplicationController@index')->name('index');
            Route::any('/home', 'ApplicationController@index');
            Route::any('/apka', 'ApplicationController@app')->name('app');
            Route::any('/apka/{angularRoute}', 'ApplicationController@app')
                ->where(['angularRoute' => '.*']);
            Route::any('/dev', 'ApplicationController@dev')->name('app_dev');
            Route::any('/prod', 'ApplicationController@prod')->name('app_prod');
            Route::any('signin', 'ApplicationController@signin')->name('signin');
            Route::any('signup', 'ApplicationController@signup')->name('signup');
            Route::any('signup-success', 'ApplicationController@signupSuccess');
            Route::any('reset-password', 'ApplicationController@resetPassword');
            Route::any('reset-password-success', 'ApplicationController@resetPasswordSuccess');
            Route::any('validate', 'ApplicationController@emailValidation');
            Route::any('404', 'ErrorController@notFound');

            Route::any('notification', 'NotificationController@notification')->middleware(SigninRequiredMiddleware::class);

            Route::any('user/publicProfile', 'ProfileController@publicProfile')->middleware(SigninRequiredMiddleware::class);
            Route::any('user/privateProfile', 'ProfileController@privateProfile')->middleware(SigninRequiredMiddleware::class);
            Route::any('user/update', 'ProfileController@update')->middleware(SigninRequiredMiddleware::class);
            // Route::post('api/user/subjects','SubjectUserController@getSubjects')->middleware(SigninRequiredMiddleware::class);

            Route::any('api/subject/join', 'SubjectUserController@join')->middleware(SigninRequiredMiddleware::class);
            Route::any('api/subject/leave', 'SubjectUserController@leave')->middleware(SigninRequiredMiddleware::class);

            Route::any('release', 'ReleaseController@index');

            Route::any('blog', 'BlogController@index');
            Route::any('blog/trello', 'BlogController@view');
        });
    }
}
