<?php

namespace Lmn\App\Veski\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class SubjectusersettingsSeeder extends Seeder {

    private function dev() {
        \DB::table('subjectusersettings')->insert([
            [
                'id' => 1,
                'subject_user_id' => 1,
                'colorpalette_id' => 1
            ],
            [
                'id' => 2,
                'subject_user_id' => 2,
                'colorpalette_id' => 4
            ],
            [
                'id' => 3,
                'subject_user_id' => 3,
                'colorpalette_id' => 7
            ],
            [
                'id' => 4,
                'subject_user_id' => 4,
                'colorpalette_id' => 13
            ]
        ]);
    }

    private function prod() {
        
    }

    public function run() {
        $env = App::environment();
        if ($env == "development") {
            $this->dev();
        } else if ($env == "production") {
            $this->prod();
        }
    }
}
