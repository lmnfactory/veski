<?php

namespace Lmn\App\Veski\Database\Seed;

use Illuminate\Database\Seeder;

class ColorpaletteSeeder extends Seeder {

    public function run() {
        \DB::table('colorpalette')->insert([
            [
                'id' => 1,
                'name' => 'red',
                'color' => 'FB6061'
            ],
            [
                'id' => 2,
                'name' => 'orange',
                'color' => 'FCA35D'
            ],
            [
                'id' => 3,
                'name' => 'yellow',
                'color' => 'F6D371'
            ],
            [
                'id' => 4,
                'name' => 'green',
                'color' => '96DB71'
            ],
            [
                'id' => 5,
                'name' => 'teel',
                'color' => '69E4ED'
            ],
            [
                'id' => 6,
                'name' => 'blue',
                'color' => '3D8CEE'
            ],
            [
                'id' => 7,
                'name' => 'purple',
                'color' => '8E7EDF'
            ],
            [
                'id' => 8,
                'name' => 'red-light',
                'color' => 'FDA6A6'
            ],
            [
                'id' => 9,
                'name' => 'orange-light',
                'color' => 'FFC493'
            ],
            [
                'id' => 10,
                'name' => 'yellow-light',
                'color' => 'FDE7AE'
            ],
            [
                'id' => 11,
                'name' => 'green-light',
                'color' => 'BFEEA6'
            ],
            [
                'id' => 12,
                'name' => 'teel-light',
                'color' => '9BF3F9'
            ],
            [
                'id' => 13,
                'name' => 'blue-light',
                'color' => '92BDFB'
            ],
            [
                'id' => 14,
                'name' => 'purple-light',
                'color' => 'BEB1F7'
            ]
        ]);
    }
}
