<?php

namespace Lmn\App\Veski\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class UsersettingsSeeder extends Seeder {

    private function dev() {
        \DB::table('usersettings')->insert([
            [
                'id' => 1,
                'user_id' => 1
            ],
            [
                'id' => 2,
                'user_id' => 2
            ],
            [
                'id' => 3,
                'user_id' => 3
            ]
        ]);
    }

    private function prod() {
        
    }

    public function run() {
        $env = App::environment();
        if ($env == "development") {
            $this->dev();
        } else if ($env == "production") {
            $this->prod();
        }
    }
}
