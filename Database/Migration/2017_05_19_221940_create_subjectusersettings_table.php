<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectusersettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjectusersettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject_user_id')->unsigned();
            $table->integer('colorpalette_id')->unsigned();
            $table->timestamps();

            $table->index('subject_user_id');
            $table->index('colorpalette_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjectusersettings');
    }
}
