<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('faculty_id')->unsigned()->nullable();
            $table->integer('degree_id')->unsigned()->nullable();
            $table->integer('studyyear')->unsigned()->nullable();
            $table->integer('gender')->unsigned()->nullable();
            $table->integer('avatar')->unsigned()->nullable();
            $table->date('birthday')->nullable();
            $table->timestamps();

            $table->index('user_id');
            $table->index('faculty_id');
            $table->index('avatar');

            //$table->foreign('avatar')->references('id')->on('file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersettings');
    }
}
