<?php

namespace Lmn\App\Veski\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Subjectusersettings extends Model {

    protected $table = 'subjectusersettings';

    protected $fillable = ['subject_user_id', 'colorpalette_id'];
}
