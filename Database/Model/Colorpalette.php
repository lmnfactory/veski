<?php

namespace Lmn\App\Veski\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Colorpalette extends Model {

    protected $table = 'colorpalette';

    protected $fillable = ['name', 'color'];
}
