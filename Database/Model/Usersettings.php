<?php

namespace Lmn\App\Veski\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\University\Database\Model\Faculty;
use Lmn\University\Database\Model\Degree;
use Lmn\File\Database\Model\File;

class Usersettings extends Model {

    protected $table = 'usersettings';

    protected $fillable = ['user_id', 'faculty_id', 'degree_id', 'studyyear', 'gender', 'birthday', 'avatar'];

    public function faculty() {
        return $this->belongsTo(Faculty::class);
    }

    public function degree() {
        return $this->belongsTo(Degree::class);
    }

    public function avatarFile() {
        return $this->belongsTo(File::class, 'avatar');
    }
}
