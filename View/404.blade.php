@extends('lmn.app-veski::layout_static')

@section('title', '404 stránka sa nenašla')

@section('css')
    <link rel="stylesheet" type="text/css" href="app/assets/css/static.css">
@endsection

@section('content')
<div class="veski-logo">
  <a href="/"><img src="app/assets/image/logo.svg" alt="veski" /></a>
</div>
<div class="main flexbox flexbox-row flexbox-row--center">
  <div class="content flexbox flexbox-column">
    <img class="book-iamge" src="app/assets/image/404-kniha.svg" alt="page not found" />
    <div>
      <h1>Stránka sa nenašla</h1>
      <h2>Vráť sa späť na <a class="highlight" href="/">Veski</a></h2>
    </div>
    @include('lmn.app-veski::footer')
  </div>
</div>
@endsection