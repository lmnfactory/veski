<div class="footer">
    <div class="flexbox flexbox-row flexbox-row--start-center">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSdEKMqL8v6vlyt2pHxFs9ivlEZUQFmtwHwsk1bn3Fl9KslEjQ/viewform" target="_blank">Kontakt a feedback</a>
    </div>
    <div class="top-offset copyright flexbox flexbox-row flexbox-row--start-center">
        <span>&copy; Veski 2017</span>
        <img  class="copyright-veski-logo" src="app/assets/image/grey_logo.svg" alt="veski" />
        <span>Vyrobené v <a href="www.lmnfactory.com">Lmn Factory</a></span>
    </div>
</div>