<?php
$texts = [];
if ($success) {
    $texts = [
        'title' => 'Teraz si overený',
        'subtitle' => 'Môžeš sa <a class="highlight" href="/signin">prihslásiť</a>',
        'icon' => 'ti-email success',
    ];
} else {
    $texts = [
        'title' => 'Email sa nepodarilo overiť',
        'subtitle' => 'Skús to neskôr',
        'icon' => 'ti-email error'
    ];
}
?>

@extends('lmn.app-veski::layout_static')

@section('title', 'Overenie emailu')

@section('css')
    <link rel="stylesheet" type="text/css" href="app/assets/css/static.css">
@endsection

@section('content')
<div class="veski-logo">
    <a href="/"><img src="app/assets/image/logo.svg" alt="veski" /></a>
</div>
<div class="main flexbox flexbox-row flexbox-row--center">
    <div class="content flexbox flexbox-column">
        <span class="{{ $texts['icon'] }} main-font-image text-center"></span>
    <div>
    <h1>{{ $texts['title'] }}</h1>
    <h2>{!! $texts['subtitle'] !!}</h2>
    </div>
        @include('lmn.app-veski::footer')
    </div>
</div>
@endsection