<!DOCTYPE html>
<html>
  <head>
        <title>Veski</title>
        <link rel="shortcut icon" type="image/ico" href="app/assets/image/veski-favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="node_modules/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="app/assets/vendor/themify-icons/themify-icons.css">
        <link rel="stylesheet" type="text/css" href="app/assets/css/mayer_reset.css">
        <link rel="stylesheet" type="text/css" href="app/assets/css/core.css">
        <link rel="stylesheet" type="text/css" href="app/assets/css/core-mobile.css">
        <link rel="stylesheet" type="text/css" href="app/assets/css/index.css">

        <base href="/">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Na Veski nájdeš aktuálny rozvrh, blížiace sa skúšky, zápočty a všetko, čo sa momentálne v škole rieši.">
        <meta name="keywords" content="škola,univerzita,študent,komunita,zápočet,cviko,cvičenie,kalendár,poznámky,skúška,rozvrh hodín,veski">

        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/js-cookie/src/js.cookie.js"></script>
    </head>

    <body>
        <div class="banner flexbox flexbox-column">
            <nav>
                <div class="flexbox flexbox-row flexbox-row--space-between-center">
                    <div class="nav-logo">
                        <img src="app/assets/image/logo.svg" />
                    </div>
                    <ul class="flexbox flexbox-row flexbox-row--center-start">
                        <li><a href="signup">Registrácia</a></li>
                        <li class="delimeter"></li>
                        <li><a href="signin">Prihlásenie</a></li>
                    </ul>
                </div>
            </nav>
            <div class="flexbox flexbox-row flexbox-row--start-center-l flexbox-row--center-s flexitem-flex">
                <div class="banner-message">
                    <h1>Spoločný priestor pre<br />všetkých študentov.</h1>
                </div>
            </div>
            <div class="flexbox flexbox-row flexbox-row--center">
                <div class="banner-button-row">
                    <a href="signup"><button type="button" class="lmn-btn banner-button">Zaregistruj sa</button></a>
                </div>
            </div>
        </div>
        <section class="white-block">
            <div class="white-block-content flexbox flexbox-row flexbox-column--xs">
                <div class="left-column top-row--xs">
                    <h2>Veski je stánka, ktorá ti pomôže organizovať tvoje štúdium.</h2>
                    <p>Na Veski nájdeš aktuálny rozvrh, blížiace sa skúšky, zápočty a všetko, čo sa momentálne v škole rieši.</p>
                </div>
            </div>
        </section>

        <section class="white-block white-block--offset">
            <div class="white-block-content flexbox flexbox-row flexbox-column--xs">
                <div class="left-column top-row--xs">
                    <h2>Samostatná stránka pre všetky tvoje predmety</h2>
                    <p>Aktuálne dianie v predmetoch. Otázky k prednáškam, poznámky k cvičeniam a ďalšie materiály. Toto všetko nájdeš v prehľadne zobrazených nástenkách.</p>
                </div>
                <div class="right-column flexbox flexbox-row flexbox-row--center bottom-row--xs">
                    <div class="column-image">
                        <span class="ti-layout-list-thumb column-icon"></span>
                    </div>
                </div>
            </div>
        </section>

        <section class="white-block white-block--offset">
            <div class="white-block-content flexbox flexbox-row flexbox-column--xs">
                <div class="left-column bottom-row--xs flexbox flexbox-row flexbox-row--center bottom-row--xs">
                    <div class="column-image">
                        <span class="ti-calendar column-icon"></span>
                    </div>
                </div>
                <div class="right-column top-row--xs top-row--xs">
                    <h2>Kalendár s tvojím rozvrhom, zápočtovkami a skúškami</h2>
                    <p>V akej miestnosti je to cvičenie? Kedy je tá skúška z matiky? Už nikdy nezabúdaj dôležitý termín. Pridávaním udalostí k predmetom si vytváraš tvoj osobný rozvrh hodín.</p>
                </div>
            </div>
        </section>

        {{--  <section class="white-block white-block--offset">
            <div class="white-block-label flexbox flexbox-row flexbox-row--center">
                <div class="white-block-label-text">Pripravujeme</div>
            </div>
            <div class="white-block-content flexbox flexbox-row flexbox-column--xs">
                <div class="left-column top-row--xs">
                    <h2>Archív minulých ročníkov</h2>
                </div>
                <div class="right-column bottom-row--xs flexbox flexbox-row flexbox-row--center">
                    <div class="column-image">
                        <span class="ti-archive column-icon"></span>
                    </div>
                </div>
            </div>
        </section>

        <section class="white-block white-block--offset">
            <div class="white-block-content flexbox flexbox-row flexbox-column--xs">
                <div class="left-column bottom-row--xs flexbox flexbox-row flexbox-row--center">
                    <div class="column-image">
                        <span class="column-icon"></span>
                    </div>
                </div>
                <div class="right-column top-row--xs">
                    <h2>Úplne zadarmo</h2>
                </div>
            </div>
        </section>

        <section class="white-block white-block--offset">
            <div class="white-block-content flexbox flexbox-row flexbox-column--xs">
                <div class="left-column top-row--xs">
                    <h2>Pripravujeme</h2>
                </div>
                <div class="right-column bottom-row--xs flexbox flexbox-row flexbox-row--center">
                    <div class="column-image">
                        <span class="ti-direction-alt column-icon"></span>
                    </div>
                </div>
            </div>
        </section>  --}}

        <section class="white-block white-block--offset">
            <div class="white-block-content flexbox flexbox-row flexbox-column--xs">
                <div class="left-column top-row--xs">
                    <h2>Pomôž nám vytvoriť niečo skutočne nové</h2>
                    <div class="flexbox flexbox-row flexbox-row--center-xs">
                        <a class="lmn-btn lmn-btn-primary" href="https://docs.google.com/forms/d/e/1FAIpQLSdEKMqL8v6vlyt2pHxFs9ivlEZUQFmtwHwsk1bn3Fl9KslEjQ/viewform" target="_blank">
                            <span class="lmn-btn-submit--default">Kontaktuj nás</span>
                            <span class="lmn-btn-submit--focus ti-angle-double-right"></span>
                        </a>
                        <a class="lmn-btn lmn-btn-primary" href="release" title="Novinky a zmeny">
                            <span class="ti-announcement"></span>
                        </a>
                        <a class="lmn-btn lmn-btn-primary" href="blog" title="Blog">
                            <span class="ti-thought"></span>
                        </a>
                    </div>
                </div>
                <div class="right-column bottom-row--xs flexbox flexbox-row flexbox-row--center">
                    <div class="column-image">
                        <span class="ti-heart column-icon"></span>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
