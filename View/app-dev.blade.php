<!DOCTYPE html>
<html>
  <head>
    <title>Veski</title>
    <link rel="shortcut icon" type="image/ico" href="/app/assets/image/veski-favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/vendor/themify-icons/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/css/mayer_reset.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/css/animation.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/css/mobile.css">

    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="/node_modules/js-cookie/src/js.cookie.js"></script>
    <!-- Polyfill(s) for older browsers -->
    <script src="/node_modules/core-js/client/shim.min.js"></script>
    <script src="/node_modules/zone.js/dist/zone.js"></script>
    <script src="/node_modules/web-animations-js/web-animations.min.js"></script>

    <script src="/node_modules/systemjs/dist/system.src.js"></script>

    <script src="/systemjs.config.js"></script>
    <script>
      System.import('app/main.js').catch(function(err){ console.error(err); });
    </script>
  </head>

  <body>
    <veski-main id="veski">
      <div class="main flexbox flexbox-row flexbox-row--center">
        <div class="veski-loading-image">
          <img src="app/assets/image/logo-notext.png" class="animation-rotate" />
        </div>
      </div>
    </veski-main>
  </body>
</html>
