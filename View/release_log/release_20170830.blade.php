@extends('lmn.app-veski::layout_static')

@section('title', 'Novinky a zmeny')

@section('css')
    <link rel="stylesheet" type="text/css" href="app/assets/css/release.css">
@endsection

@section('meta')
    <meta name="description" content="Zmeny a novinky, na stranke Veski pisane priamo vyvojarmi pre studentov rozdelene do troch kategorii.">
    <meta name="keywords" content="škola,univerzita,študent,komunita,zápočet,cviko,cvičenie,kalendár,poznámky,skúška,rozvrh hodín,veski,novinky,zmeny,opravy,vypustenie,poznamky,vyvojarsky dennik">
@endsection

@section('content')
    <div class="main flexbox flexbox-row flexbox-row--start-center">
        <div class="center-column flexbox flexbox-column">
            <div class="release-content flexbox flexbox-column">
                <div class="nav-logo">
                    <a href="/" title="Domov"><img src="app/assets/image/logo.svg" /></a>
                </div>
                <h1>Novinky a zmeny</h1>
                <div class="release-day">
                    <span class="release-date">08.10. 2017</span>
                    <section class="release-block">
                        <h2>Nové šmaky <span class="success">#newfeatures</span></h2>
                        <ul>
                            <li><span class="ti-angle-right ul-style"></span>Je možné klikať na celý riadok udalosti v zozname udalostí pod kalendárom.</li>
                            <li><span class="ti-angle-right ul-style"></span>Skús prejsť myšou nad ikony <span class="ti-files space-left-8" title="súbory"></span><span class="ti-comment-alt space-left-8" title="príspevky"></span><span class="ti-user space-left-8" title="členovia"></span>. Pridali sme pomocné texty na ikony.</li>
                            <li><span class="ti-angle-right ul-style"></span>Zmenená url-ka na hlavnú stránku. už nie je treba písať /home.</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Zmeny, ktoré nikto nemá rád <span class="success">#design</span></h2>
                        <ul>
                            <li><span class="ti-angle-right ul-style"></span>Každá téma v predmete je vo vlastnom bielom bloku. Témi sú vďaka tomu lepšie oddelené a snáť aj viac čitateľné.</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Pár dôvodov na kludnejší spánok <span class="success">#bugfix</span></h2>
                        <ul>
                            <li><span class="ti-angle-right ul-style"></span>Na registračnej stránke sa objavoval anglický text. Všetko by už malo byť preložené.</li>
                            <li><span class="ti-angle-right ul-style"></span>Viaceré stránky mali rozbité formátovanie textu. Všetko už je v poriadku. Išlo o stránky po úspešnej registrácií, po zmene helsa a po potvrdení emailu.</li>
                            <li><span class="ti-angle-right ul-style"></span>Pridávanie nových udalostí nešlo ... vôbec. Neverím, že táto chyba nebola nahlásená skôr. Už je to opravené a pripravili sme testy aby sa to neopakovalo.</li>
                            <li><span class="ti-angle-right ul-style"></span>Vo firefoxe nebolo vidno celý text vo formulári. Už si nemusíš domýšlať či pismeno bolo "g" alebo "q" pretože už je vidno aj spodná časť textu.</li>
                            <li><span class="ti-angle-right ul-style"></span>Najdôležitejšia oprava. SCROLLOVANIE FUNGUJE. Stránka bola kompletne nepoužiteľná. Naštasie toto smutné obdobie je zanami.</li>
                        </ul>
                    </section>
                </div>
                <div class="release-day">
                    <span class="release-date">02.10. 2017</span>
                    <section class="release-block">
                        <h2>Nové šmaky <span class="success">#newfeatures</span></h2>
                        <ul>
                            <li><span class="ti-angle-right ul-style"></span>Koho štvalo, že sa nemohol prekliknúť z kalendára na udalosť? už sa to konečné dá, ale len pre udalosti, ktoré sú pripojené na predmet. Môžeš to vyskúšať v týždennom zobrazení kalendára a v zozname udalstí na konkrétny deň pod kalendárom.</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Pár dôvodov na kludnejší spánok <span class="success">#bugfix</span></h2>
                        <ul>
                            <li><span class="ti-angle-right ul-style"></span>Text v menu na mobile sa nechcel zobrazovať celý. Donútili sme ho zobrazovať sa celý.</li>
                        </ul>
                    </section>
                </div>
                <div class="release-day">
                    <span class="release-date">30.09. 2017</span>
                    <section class="release-block">
                        <h2>Nové šmaky <span class="success">#newfeatures</span></h2>
                        <ul>
                            <li><span class="ti-angle-right ul-style"></span>Evidentne sa veľa ľudí chybilo pri vytváraní udalosti. Konečne máte možnosť si po sebe, a po druhých, negramotných, ľuďoch opraviť chyby. Verím, že srdiečko každého grammer-nazi zaplesá pri tejto zmene.</li>
                            <li><span class="ti-angle-right ul-style"></span>V texte threadu sa budú zvýrazňovať hashtagy. Toto zlepší používanie threadov na mobile, kedže na mobile neboli nikde vidno tagy. <span class="highlight-word">#hashtag</span></li>
                            <li><span class="ti-angle-right ul-style"></span>Text v threade a komente detekuje odkazy. Stačí skopírovať link do textu a ona sa po odoslaní upraví na odkaz.</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Zmeny, ktoré nikto nemá rád <span class="success">#design</span></h2>
                        <ul>
                            <li><span class="ti-angle-right ul-style"></span>Tlačítko na pridanie a opustenie udalostí sme nahradili za <span class="lmn-more lmn-ti-vertical"><span class="ti-more-alt"></span></span></li>
                            <li><span class="ti-angle-right ul-style"></span>Vyhľadávacie pole získalo menší, kompaktnejší vzhľad. Rovnaký ako je v hlavičke ktorá sa nalepí navrch pri skrolovaní.</li>
                            <li><span class="ti-angle-right ul-style"></span>Zmenšili sme medzeri medzi predmetami v založke predmety. Každá správna medzera má mať 16 alebo 8 pixelov. Takéto rebelské 24 pixelové medzeri na stránke nestrpíme.</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Pár dôvodov na kludnejší spánok <span class="success">#bugfix</span></h2>
                        <ul>
                            <li><span class="ti-angle-right ul-style"></span>Pre mobilných hipsterov sme opravili problém s otváranim kalendáru po kliknutí na tlčítko <span class="guide">pridaj</span> na stránke udalostí k predmetu.</li>
                            <li><span class="ti-angle-right ul-style"></span>Zaškrtávacie tlačítko "zapamatať prihlásenie" má opäť zmysel, pretože konečne funguje.</li>
                            <li><span class="ti-angle-right ul-style"></span>Opravené serverové wazy-wuzy. Server bude vďaka ním bežať lepšie.</li>
                        </ul>
                    </section>
                </div>
            </div>
            @include('lmn.app-veski::footer')
        </div>
    </div>    
@endsection
