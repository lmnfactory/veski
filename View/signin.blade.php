@extends('lmn.app-veski::layout_form')

@section('title', 'Veski | Prihlásenie')
@section('form_title', 'Prihlásenie')

@section('form_meta')
    <meta name="description" content="Prihlasovací formulár do sociálnej siete pre študentov.">
@endsection

@section('form_js')
    <script src="app/assets/js/signin.js"></script>
@endsection

@section('form')
    <form novalidate onsubmit="return signin(event)">
        <div validation="email">
            <div class="validation-error-label" val-message="required">Email nám musíš vyplniť</div>
            <div class="validation-error-label" val-message="email">Každý správny email musí mať '@'' a '.'</div>
            <div class="validation-error-label" val-message="server"></div>
        </div>
        <div validation="password">
            <div class="validation-error-label" val-message="required">Heslo nám musíš vyplniť</div>
            <div class="validation-error-label" val-message="server"></div>
        </div>
        <div class="lmn-input-container" validation="email">
            <input type="email" name="email" placeholder="Email" />
        </div>
        <div class="lmn-input-container" validation="password">
            <input type="password" name="password" placeholder="Heslo" />
        </div>
        <div class="lmn-input-container">
            <input id="rememberme" type="checkbox" name="rememberme" value="1" />
            <label for="rememberme">Zapamätať prihlásenie</label>
        </div>
        <button typ="submit" class="lmn-btn-submit">
            <span class="lmn-btn-submit--default">Prihlásiť sa</span>
            <span class="lmn-btn-submit--focus ti-check"></span>
        </button>
    </form>
@endsection

@section('footer')
    <div class="center">
        <img class="veski-logo-delimeter" src="app/assets/image/grey_logo.svg" alt="veski" />
    </div>
    <div>
        <p class="offset-top">Ešte nemáš vytvorený účet? <a class="highlight" href="signup">Zaregistruj sa.</a></p>
        <p class="offset-top">Nevieš svoje heslo? <a class="highlight" href="reset-password">Požiadaj o zmenu hesla.</a></p>
    </div>
@endsection