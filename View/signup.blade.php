@extends('lmn.app-veski::layout_form')

@section('title', 'Veski | Registrácia')
@section('form_title', 'Registrácia')
    
@section('form_meta')
    <meta name="description" content="Registračný formulár do sociálnej siete pre študentov.">
@endsection

@section('form_js')
    <script src="app/assets/js/signup.js"></script>
@endsection

@section('form')
    <form novalidate onsubmit="return signup(event)">
        <div validation="name">
            <div class="validation-error-label" val-message="required">Meno nám musíš vyplniť</div>
            <div class="validation-error-label" val-message="maxLength">To je dosť dlhé meno, budeš ho musieť skratiť na 128 znakov.</div>
        </div>
        <div validation="surname">
            <div class="validation-error-label" val-message="required">Priezvisko nám musíš vyplniť</div>
            <div class="validation-error-label" val-message="maxLength">To je dosť dlhé priezvisko, budeš ho musieť skratiť na 128 znakov.</div>
        </div>
        <div validation="email">
            <div class="validation-error-label" val-message="required">Email nám musíš vyplniť</div>
            <div class="validation-error-label" val-message="maxLength">To je dosť dlhý email, budeš ho musieť skratiť na 128 znakov.</div>
            <div class="validation-error-label" val-message="email">Každý správny email musí mať '@'' a '.'</div>
            <div class="validation-error-label" val-message="server"></div>
        </div>
        <div validation="password">
            <div class="validation-error-label" val-message="required">Heslo nám musíš vyplniť</div>
            <div class="validation-error-label" val-message="maxLength">To je dosť dlhé heslo, budeš ho musieť skratiť na 40 znakov.</div>
            <div class="validation-error-label" val-message="minLength">To je príliš krátke heslo, budeš ho musieť rozšíriť aspon na 6 znakov.</div>
        </div>
        <div validation="agreement">
            <div class="validation-error-label" val-message="checked">Musíš súhlasíť s podmienkami</div>
        </div>
        
        <div class="lmn-input-container" validation="name">
            <input type="text" name="name" placeholder="Meno" />
        </div>
        <div class="lmn-input-container" validation="surname">
            <input type="text" name="surname" placeholder="Priezvisko" />
        </div>
        <div class="lmn-input-container" validation="email">
            <input type="email" name="email" placeholder="Email" />
        </div>
        <div class="lmn-input-container" validation="password">
            <input type="password" name="password" placeholder="Heslo" />
        </div>
        <div class="lmn-input-container" validation="agreement">
            <input id="agreement" type="checkbox" name="agreement" />
            <label for="agreement">Súhlasím s <a class="highlight" href="">podmienkami používania</a> a zásadami <a class="highlight" href="">ochrany súkromia</a> Veski.</label>
        </div>
        <button typ="submit" class="lmn-btn-submit">
            <span class="lmn-btn-submit--default">Registrovať sa</span>
            <span class="lmn-btn-submit--focus ti-check"></span>
        </button>
    </form>
@endsection

@section('footer')
    <div class="center">
        <img class="veski-logo-delimeter" src="app/assets/image/grey_logo.svg" alt="veski" />
    </div>
    <div>
        <p class="offset-top">Už máš vytvorený účet? <a class="highlight" href="signin">Prihlás sa.</a></p>
    </div>
@endsection