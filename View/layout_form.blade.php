@extends('lmn.app-veski::layout_static')

@section('meta')
    <meta name="robots" content="noindex,follow">
    @yield('form_meta')
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="app/assets/css/static.css">
    @yield('form_css')
@endsection

@section('js')
    <script src="app/assets/js/app.js"></script>
    <script src="app/assets/js/validation.js"></script>
    <script src="app/assets/js/form.js"></script>
    <script src="app/assets/js/boot.js"></script>
    @yield('form_js')
@endsection

@section('content')
    <div class="main flexbox flexbox-row">
        <div class="form content flexbox flexbox-column flexitem-center">
            <div class="form-box">
                <div class="veski-logo-form">
                    <a href="/"><img src="app/assets/image/logo-notext.png" alt="Veski logo" /></a>
                </div>
                <h2>@yield('form_title')</h2>
                @yield('form')
            </div>
            <div class="form-footer">
                @yield('footer')
            </div>
        </div>
    </div>    
@endsection
