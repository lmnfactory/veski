@extends('lmn.app-veski::layout_form')

@section('title', 'Veski | Obnovenie hesla')
@section('form_title', 'Obnovenie hesla')
    
@section('form_meta')
    <meta name="description" content="Formulár pre obnovu hesla do sociálnej siete pre študentov.">
@endsection

@section('form_js')
    <script src="app/assets/js/reset_password.js"></script>
@endsection

@section('form')
    <form novalidate onsubmit="return resetPassword(event)">
        <div validation="email">
            <div class="validation-error-label" val-message="required">Email nám musíš vyplniť</div>
            <div class="validation-error-label" val-message="email">Každý správny email musí mať '@' a '.'</div>
            <div class="validation-error-label" val-message="server"></div>
        </div>
        <div class="lmn-input-container" validation="email">
            <input type="email" name="email" placeholder="Email" />
        </div>
        <button typ="submit" class="lmn-btn-submit">
            <span class="lmn-btn-submit--default">Obnoviť heslo</span>
            <span class="lmn-btn-submit--focus ti-check"></span>
        </button>
    </form>
@endsection

@section('footer')
    <div class="center">
        <img class="veski-logo-delimeter" src="app/assets/image/grey_logo.svg" alt="veski" />
    </div>
    <div>
        <p class="offset-top">Vieš svoje heslo? <a class="highlight" href="signin">Prihlás sa.</a></p>
    </div>
@endsection