@extends('lmn.app-veski::blog.layout.list')

@section('title', 'Blog')

@section('meta')
    <meta name="description" content="Blog organizácie veski, zameraný na vývoj webových stránok, design a život ľudí, ktorý robia niečo pre túto planétu.">
    <meta name="keywords" content="veski,blog,článok,myšlienky,zlepšenia,napady,design,vývoj,web,webový vývoj,ľudia">
@endsection

@section('content')
    @foreach($list as $item)
        @include('lmn.app-veski::blog.component.preview', [
            'article' => $item
        ])
    @endforeach
@endsection