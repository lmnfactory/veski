<!DOCTYPE html>
<html>
  <head>
        @include('lmn.app-veski::component.google_analytics')

        <title>@yield('title')</title>
        <link rel="shortcut icon" type="image/ico" href="/app/assets/image/veski-favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="/node_modules/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/app/assets/vendor/themify-icons/themify-icons.css">
        <link rel="stylesheet" type="text/css" href="/app/assets/css/mayer_reset.css">
        <link rel="stylesheet" type="text/css" href="/app/assets/css/core.css">
        <link rel="stylesheet" type="text/css" href="/app/assets/css/core-mobile.css">
        <link rel="stylesheet" type="text/css" href="/app/assets/css/blog.css">
        @yield('css')

        <base href="/">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="škola,univerzita,študent,komunita,zápočet,cviko,cvičenie,kalendár,poznámky,skúška,rozvrh hodín,veski">

        @yield('meta')

        @yield('js')
    </head>

    <body>
        <div class="main flexbox flexbox-row flexbox-row--start-center">
            <div class="center-column flexbox flexbox-column flexbox-ellipsis-fix">
                <div class="flexbox flexbox-column">
                    <header class="flexbox flexbox-row flexbox-row--center-between">
                        <div class="flexbox-ellipsis-fix">
                            <h1 class="ellipsis">Blog</h1>
                        </div>
                        <div class="nav-logo">
                            <a class="link--image" href="/" title="Domov"><img src="app/assets/image/logo.svg" /></a>
                        </div>
                    </header>
                    @yield('content')
                </div>
            </div>
        </div>
    </body>
</html>
