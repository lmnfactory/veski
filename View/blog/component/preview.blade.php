<section class="preview blog-content">
    <h1><a class="link--dark" href="{{ $item['url'] }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a></h1>
    <div class="flexbox flexbox-column--xs flexbox-row">
        <div class="flexbox flexbox-row flexbox-row--center">
            <div class="thumbnail">
                <a class="link--image" href="{{ $item['url'] }}" title="{{ $item['title'] }}">
                    <img src="{{ $item['thumbnail'] }}">
                </a>
            </div>
        </div>
        <div>
            <p>{{ $item['text'] }} <a href="{{ $item['url'] }}" title="{{ $item['title'] }}">chcem vedieť viac</a></p>
        </div>
    </div>
</section