<ul>
    @foreach($items as $item)
        <li class="flexbox flexbox-row"><span class="ti-angle-right ul-style"></span><span class="list-text">{!! $item['text'] !!}</span></li>
    @endforeach
</ul>