<figure>
    <a class="link--image" href="{{ $imgUrl }}" target="_blank"><img src="{{ $imgUrl }}" alt="{{ $imgName }}"></a>
    <figcaption>{{ $caption }}</figcaption>
</figure>