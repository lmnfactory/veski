@extends('lmn.app-veski::blog.layout.basic')

@section('title', 'Trello')

@section('meta')
    <meta name="description" content="Prvé zoznámenie s nástrojom trello a príprava nástenky pre web vývoj.">
    <meta name="keywords" content="trello,návod,web,vývoj,tím,začiatok,začíname,ako na,úlohy,organizácia,organizácia úloh,blog,veski">
@endsection

@section('blog_title', 'Trello pre začínajúci projekt')
@section('author', '@pipan')
@section('date', 'jeseň 2017')

@section('content')
    @include('lmn.app-veski::blog.component.heading', ['text' => 'Trello'])
    @include('lmn.app-veski::blog.component.paragraph', [
        'text' => 'Trello je krásny nástroj na organizovanie projektu. Nedostatkom je, že ti nikto neukáže ako si ho nastaviť. Existujú preset, ale nikdy som k ním nenašiel vysvetlenie prečo je to takto dobré alebo návod ako by sa mal používať. Preto sa pokusim v tomto článku ukázť, do detailu, preset na vývoj webovej aplikácie v malej skupine ľudí.'
    ])
    @include('lmn.app-veski::blog.component.paragraph', [
        'text' => 'Tento preset sme skonštruovali počas našej práce na projekte <a href="www.veski.sk">veski</a>. Pracovali sme na ňom traja, ale trello sme v podstate používali len dvaja. Kamoš mal s trellom skúsenosti a preto nám dal úvod do tejto aplikácie. Zo začiatku som bol trochu stratený ako používať tento nástroj. Žiaden ovládací prvok som si nevedel spojiť s doterajšími aplikáciami ktoré som používal, či už word, photoshop, IDE, google keep, … Preto sme týmto ovládacím prvkom priradili účel, ktorý sa nám zdal  byť najvhodnejší.'
    ])

    @include('lmn.app-veski::blog.component.heading', ['text' => 'Súčasti'])
    @include('lmn.app-veski::blog.component.unordered_list', [
        'items' => [
            [
                'text' => '<b>Zoznam</b> reprezentuje v akej fáze vývoja sa úloha nachádza. Môže to byť napríklad (in progress, done, to do, on hold, backloged, …). <a href="' . $imagePath . 'zoznam.png" target="_blank">ukážka<span class="ti-new-window offset-left-whitespace"></span></a>'
            ], [
                'text' => '<b>Karta</b> je samotná úloha. Na vytvorenie podúloh stačí použiť zaškrtávacie polia v rámci úlohy. <a href="' . $imagePath . 'karty.png" target="_blank">ukážka<span class="ti-new-window offset-left-whitespace"></span></a>'
            ], [
                'text' => '<b>Značka</b> je kategória, do ktorej úloha patrí (napr. design, wording, server, bugfix, dokumentácia, … ). Takto môže byť karta vo viacerých kategóriach súčastne. <a href="' . $imagePath . 'znacky.png" target="_blank">ukážka<span class="ti-new-window offset-left-whitespace"></span></a>'
            ], [
                'text' => '<b>Horizontálna os</b>, alebo zoradenie listov z ľava do prava, predstavuje časový posun úlohy. To znamená, že úloha začína vľavo, napríklad ako „idea“ a posúva sa počas svojho života doprava, napríklad na „to do“ →  „in progress“ → „done“ → … <a href="' . $imagePath . 'horizontalna_os.png" target="_blank">ukážka<span class="ti-new-window offset-left-whitespace"></span></a>'
            ], [
                'text' => '<b>Vertikálna os</b> v jednom zozname môže slúžiť na určenie priority alebo na určenie kedy bola úloha pridaná do daného zoznamu. Niekedy je dobré mať prehľad o prioritách a niekedy o tom, kedy do zoznamu úloha pribudla. V našom projekte sme mali v zozname „done“ úlohy zoradene podľa toho, kedy boli pridané do tohoto zoznamu, aby sme z toho vedeli spätne vytvoriť správu o tom, aké úlohy sme v poslednej dobe dokončili. Na druhú stranu, zoznam „todo“ sme mali zoradený podľa priority, pretože niektoré veci boli naozaj dôležité a človek ich chcel v takom prípade vidieť vždy na vrchu. <a href="' . $imagePath . 'vertikalna_os.png" target="_blank">ukážka<span class="ti-new-window offset-left-whitespace"></span></a>'
            ]
        ]
    ])
    @include('lmn.app-veski::blog.component.figure', [
        'imgUrl' => $imagePath . 'borad_parts.png',
        'imgName' => 'ukážka nástenky trella',
        'caption' => 'Ukážka nástenky trella'
    ])

    @include('lmn.app-veski::blog.component.paragraph', [
        'text' => 'Tento preset bol pre nás najstabilnejší a vydržal nám počas celého projektu. Neskôr som sa v práci zoznámil s <a href="https://asana.com/">asanou</a>.V nej sme používali možnosť priraďovať ľudí na úlohu. Takto má každý lepší prehľad o tom, na čom má robiť, a na ktoré úlohy nie je nikto priradený. Takýto system je stabilnejší vo vačšej skupine ľudí a vyžaduje si menšiu znalosť toho ako funguje celý proces zadávania úloh. Každé oddelenie sa stará len o svoje úlohy.'
    ])
    @include('lmn.app-veski::blog.component.note', [
        'text' => 'asana - aplikícia na organizáciu úloh'
    ])

    @include('lmn.app-veski::blog.component.heading', ['text' => 'Pre vačší tím'])
    @include('lmn.app-veski::blog.component.paragraph', [
        'text' => 'Poďme si teraz ukázať ako používať trello na spôsob priradzovania úloh jednotlivým ľuďom.  Každá karta v trelle môže byť priradená na ľudí a každý používateľ si môže v trelle filtrovať úlohy aj pomocou podmienky priradené mne alebo nepriradené. Je to veľmi efektívny spôsob, ako mať prehľad o projekte a zároveň nezabíjať ostatné možnosti (ako sú značky, vertikálne a horizontálne osi a zoznami). Ukážme si, ako by vyzeral projekt pre malý tim v trelle.'
    ])
    @include('lmn.app-veski::blog.component.unordered_list', [
        'items' => [
            [
                'text' => '<b>Zoznami</b> budú reprezentovať kanále. Kanal je v podstate oddelenie / kategória. Každá úloha môže byť len v jednom kanály. Ukážka dobrých zoznamov „ideas“, „backlog“, “done“,  „design“, „wording“, „code“, „ux“. Môžeme vydieť, že niektoré kategórie z minulého presetu sa stali zoznamom pre tento preset.'
            ], [
                'text' => '<b>Karta</b> stále zostáva úloha'
            ], [
                'text' => '<b>Horizontálna os</b> nemá v tomto presete žiaden význam'
            ], [
                'text' => '<b>Vertikálnu os</b> odporúčam používať na udržiavanie poradia úloh ako pribudnú do zoznamu. Najnovšie úlohy budú na vrchu. V tomto presete sa budú často používať filtre, preto nemá veľký zmysel zachovávať prioritu ako hodnotu pre vertikálnu os.'
            ], [
                'text' => '<b>Značka</b> je najvariabilnejšia zložka tohoto presetu. Značka môže reprezentovať dodatočné kategórie, tagy alebo prioritu. Ja sa najviac prikláňam k tomu, aby značka reprezentovala prioritu. Nemá zmysel používať značku ako kategóriu, pretože samotné zoznami (kanale) môžu fungovať ako kategórie,. Tagy su prilis všeobecné a treba ich najskôr definovať, aby si používatelia nevimýšlali vlastné. Pre tieto dôvody by som používal značku ako prioritu. Napríklad low, high medium alebo minor, major, critical, blocker, bugfix, duplicate,'
            ]
        ]
    ])

    @include('lmn.app-veski::blog.component.heading', ['text' => 'Záverečné slová'])

    @include('lmn.app-veski::blog.component.paragraph', [
        'text' => 'Používanie takéhoto systému je trochu iné ako prvého presetu. Teraz budem viac používať priradzovanie ľudí na úlohy, pričom úloha počas svojho života zostane v podstate v jednom zozname. Až keď sa úloha ukončí, tak sa presunie do zoznamu „done“. Každí používateľ si vie vyfiltrovať karty, na ktorých je priradený. Ak je priradený na karte, ktorá je mimo jeho kanálu (napríklad ak je designer priradený na karte, ktorá je v zozname „code“, to znamená, že má programátor nejakú otázku na designera a bude to rýchla záležitosť). Ak je priradený na karte, ktorá je v jeho zozname, tak to je jeho „hlavná“ úloha. Tak isto si vie rýchlo nájst úlohu zo svojho kanálu, podľa toho, ktorá nie je priradená na nikoho. Po dokončení úlohy, presunie úlohu na iný kanál alebo ho vie presunúť do zoznamu „done“. Podľa mňa je vidno, že v takejto štruktúre sa vie zorientovat každý človek z tímu lepšie ako v prvom presete. Preto je prvý preset zaradený ako vhodný pre 1-3 ludí a druhý preset vhodný pre 3-15 ludi.'
    ])
@endsection