<!DOCTYPE html>
<html>
  <head>
        @include('lmn.app-veski::component.google_analytics')

        <title>@yield('title')</title>
        <link rel="shortcut icon" type="image/ico" href="app/assets/image/veski-favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="node_modules/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="app/assets/vendor/themify-icons/themify-icons.css">
        <link rel="stylesheet" type="text/css" href="app/assets/css/mayer_reset.css">
        <link rel="stylesheet" type="text/css" href="app/assets/css/core.css">
        <link rel="stylesheet" type="text/css" href="app/assets/css/core-mobile.css">
        @yield('css')

        <base href="/">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="škola,univerzita,študent,komunita,zápočet,cviko,cvičenie,kalendár,poznámky,skúška,rozvrh hodín,veski">

        @yield('meta')

        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/js-cookie/src/js.cookie.js"></script>

        @yield('js')
    </head>

    <body>
        @yield('content')
    </body>
</html>
