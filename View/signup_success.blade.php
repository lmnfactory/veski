@extends('lmn.app-veski::layout_static')

@section('title', 'Úspešná registrácia')

@section('css')
    <link rel="stylesheet" type="text/css" href="app/assets/css/static.css">
@endsection

@section('content')
    <div class="veski-logo">
      <a href="/"><img src="app/assets/image/logo.svg" alt="veski" /></a>
    </div>
    <div class="main flexbox flexbox-row flexbox-row--center">
      <div class="content flexbox flexbox-column">
        <span class="ti-check success main-font-image text-center"></span>
        <div>
          <h1>Registrácia prebehla úspešne!</h1>
          <h2>Na email sme ti poslali aktivačný link, po jeho kliknutí sa už budeš môcť <a class="highlight" href="/signin">prihlásiť</a>.</h2>
        </div>
        @include('lmn.app-veski::footer')
      </div>
    </div>
@endsection
