@extends('lmn.app-veski::layout_static')

@section('title', 'Úspešné obnovenie hesla')

@section('css')
    <link rel="stylesheet" type="text/css" href="app/assets/css/static.css">
@endsection

@section('content')
<div class="veski-logo">
  <a href="/"><img src="app/assets/image/logo.svg" alt="veski" /></a>
</div>
<div class="main flexbox flexbox-row flexbox-row--center">
  <div class="content flexbox flexbox-column">
    <span class="ti-key success main-font-image text-center"></span>
    <div>
      <h1>Nové heslo</h1>
      <h2>ti už cestuje na zadaný email. Najlepšie bude, ak si ho hneď po <a class="highlight" href="/signin">prihlásení</a> zmeníš.</h2>
    </div>
    @include('lmn.app-veski::footer')
  </div>
</div>
@endsection