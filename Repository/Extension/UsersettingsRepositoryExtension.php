<?php

namespace Lmn\App\Veski\Repository\Extension;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\AbstractEloquentRepositoryExtension;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\App\Veski\Database\Model\Usersettings;
use Lmn\App\Veski\Repository\UsersettingsRepository;

class UsersettingsRepositoryExtension extends AbstractEloquentRepositoryExtension {

    public function __construct(UsersettingsRepository $usersettingsRepo) {
        parent::__construct($usersettingsRepo);
    }

    public function create($data) {
        if (!isset($data['user_id'])) {
            return null;
        }
        if (!isset($data['usersettings'])) {
            $data['usersettings'] = [];
        }

        $usersettingsData = $data['usersettings'];
        $usersettingsData['user_id'] = $data['user_id'];

        return parent::create($usersettingsData);
    }

    public function update($data) {
        if (!isset($data['usersettings'])) {
            $data['usersettings'] = [];
        }

        $usersettingsData = $data['usersettings'];

        return parent::update($usersettingsData);
    }
}
