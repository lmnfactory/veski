<?php

namespace Lmn\App\Veski\Repository;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\App\Veski\Database\Model\Usersettings;

class UsersettingsRepository extends AbstractEloquentRepository {

    public function __construct(CriteriaService $criteriaService) {
        parent::__construct($criteriaService);
    }

    public function getModel() {
        return Usersettings::class;
    }

    public function get() {
        $this->criteria('usersettings.permission', ['permission' => $this->getPermission()]);
        return parent::get();
    }

    public function create($data) {
        $model = $this->getModel();

        $usersettings = new $model();
        $usersettings->fill($data);

        $usersettings->save();

        return $usersettings;
    }
}
