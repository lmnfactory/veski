<?php

namespace Lmn\App\Veski\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectExtensionCountCriteria implements Criteria {

    private $subjectId;

    public function __construct() {

    }

    public function set($args) {
        
    }

    public function apply(Builder $builder) {
        $builder->selectRaw('COUNT(*) as count, subject_id')
            ->groupBy('subject_id');
    }
}
