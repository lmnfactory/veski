<?php

namespace Lmn\App\Veski\Repository\Criteria;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UsersettingsPermissionCriteria implements Criteria {

    private $permission;

    public function __construct() {

    }

    public function set($args = []) {
        $this->permission = $args['permission'];
    }

    public function apply(Builder $builder) {
        if ($this->permission == 'owner') {

        }
        else {
            $builder->select(['usersettings.user_id', 'usersettings.faculty_id', 'usersettings.degree_id', 'usersettings.studyyear', 'usersettings.avatar']);
        }
    }
}
