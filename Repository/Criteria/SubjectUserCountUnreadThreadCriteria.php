<?php

namespace Lmn\App\Veski\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectUserCountUnreadThreadCriteria implements Criteria {

    private $subjectId;

    public function __construct() {

    }

    public function set($args) {

    }

    public function apply(Builder $builder) {
        $builder->selectRaw('COUNT(*) as count, subject_user.subject_id')
            ->join('subject', 'subject.id', '=', 'subject_user.subject_id')
            ->join('thread', 'thread.subject_id', '=', 'subject.id')
            ->where('thread.created_at', '>', 'subject_user.lastvisit')
            ->groupBy('subject_id');
    }
}
