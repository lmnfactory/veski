<?php

namespace Lmn\App\Veski\Repository\Listener;

use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\AbstractListenerEloquentRepository;

class SubjectusersettingsListener extends AbstractListenerEloquentRepository {

    private $repo;

    public function __construct(EloquentRepository $repo) {
        $this->repo = $repo;
    }

    public function onCreate($model, $data) {
        if (!isset($data['usersettings'])) {
            $data['usersettings'] = [];
        }
        $settings = $data['usersettings'];
        $settings['subject_user_id'] = $model->id;
        $this->repo->clear()
            ->create($settings);
    }

    public function onGet($model) {
        if (!$model->usersettings) {
            return;
        }

        $model->usersettings->settings = $this->repo->clear()
            ->criteria('subjectuser.ext.id', ['subjectuserId' => $model->id])
            ->get();
    }

    public function onList($models) {
        $ids = [];
        foreach ($models as $m) {
            if (!isset($m->usersettings)) {
                continue;
            }
            $ids[] = $m->usersettings->id;
        }

        $list = $this->repo->clear()
            ->criteria('subjectuser.ext.ids', ['subjectuserIds' => $ids])
            ->all();

        $listSorted = [];
        foreach ($list as $l) {
            $listSorted[$l->subject_user_id] = $l;
        }
        foreach ($models as $model) {
            if (!isset($model->usersettings)) {
                continue;
            }
            if (!isset($listSorted[$model->usersettings->id])) {
                $model->usersettings->settings = [];
                continue;
            }
            $model->usersettings->settings = $listSorted[$model->usersettings->id];
        }
    }
}
