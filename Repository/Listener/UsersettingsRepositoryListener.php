<?php

namespace Lmn\App\Veski\Repository\Listener;

use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\AbstractListenerEloquentRepository;

class UsersettingsRepositoryListener extends AbstractListenerEloquentRepository {

    private $repo;

    public function __construct(EloquentRepository $repo) {
        $this->repo = $repo;
    }

    public function onCreate($model, $data) {
        if (!isset($data['settings'])) {
            $data['settings'] = [];
        }
        $settings = $data['settings'];
        $settings['user_id'] = $model->id;
        $this->repo->clear()
            ->create($settings);
    }

    public function onUpdate($model, $data) {
        if (!isset($data['settings'])) {
            return;
        }
        $settings = $data['settings'];
        $settings['user_id'] = $model->id;
        $this->repo->clear()
            ->criteria('user.ext.id', ['userId' => $model->id])
            ->update($settings);
    }

    public function onGet($model) {
        $model->settings = $this->repo->clear()
            ->criteria('user.ext.id', ['userId' => $model->id])
            ->criteria('usersettings.with.all')
            ->get();
    }

    public function onList($models) {
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }

        $settings = $this->repo->clear()
            ->criteria('user.ext.ids', ['userIds' => $ids])
            ->criteria('usersettings.with.all')
            ->all();
        
        $settingsIndexed = [];
        foreach ($settings as $s) {
            $settingsIndexed[$s->user_id] = $s;
        }

        foreach ($models as $m) {
            if (isset($settingsIndexed[$m->id])) {
                $m->settings = $settingsIndexed[$m->id];
            }
        }
    }
}
