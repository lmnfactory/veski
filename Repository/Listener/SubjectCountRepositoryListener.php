<?php

namespace Lmn\App\Veski\Repository\Listener;

use Lmn\Core\Lib\Repository\AbstractListenerEloquentRepository;
use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;

class SubjectCountRepositoryListener extends AbstractListenerEloquentRepository {

    private $key;
    private $repo;

    public function __construct($key, EloquentRepository $repo) {
        $this->key = $key;
        $this->repo = $repo;
    }

    public function onGet($model) {
        $index = $this->key;
        $repoCount = $this->repo->clear()
            ->criteria('subject.ext.id', ['subjectId' => $model->id])
            ->criteria('subject.ext.count')
            ->get();

        if ($repoCount == null) {
            return 0;
        }
       $model->$index = $repoCount->count;
    }

    public function onList($models) {
        $index = $this->key;
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }

        $list = $this->repo->clear()
            ->criteria('subject.ext.count')
            ->criteria('subject.ext.ids', ['subjectIds' => $ids])
            ->all();

        $listSorted = [];
        foreach ($list as $l) {
            $listSorted[$l->subject_id] = $l;
        }
        foreach ($models as $model) {
            if (!isset($listSorted[$model->id])) {
                $model->$index = 0;
                continue;
            }
            $model->setAttribute($index, $listSorted[$model->id]->count);
        }
    }
}
