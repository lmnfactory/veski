<?php

namespace Lmn\App\Veski\Repository;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\App\Veski\Database\Model\Subjectusersettings;

class SubjectusersettingsRepository extends AbstractEloquentRepository {

    public function __construct(CriteriaService $criteriaService) {
        parent::__construct($criteriaService);
    }

    public function getModel() {
        return Subjectusersettings::class;
    }

    public function create($data) {
        $model = $this->getModel();

        $subjectusersettings = new $model();
        $subjectusersettings->fill($data);

        $subjectusersettings->save();

        return $subjectusersettings;
    }
}
