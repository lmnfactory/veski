<?php

namespace Lmn\App\Veski\Lib\Exception;

use Lmn\Core\Lib\Exception\ExceptionHandler;
use Lmn\Core\Exception\SystemException;
use Lmn\Core\Lib\Response\ResponseService;

class NotFoundExceptionHandler implements ExceptionHandler {

    public function __construct() {

    }

    public function report(\Exception $ex) {

    }

    public function render($request, \Exception $ex, ResponseService $responseService) {

        return response()->view('lmn.app-veski::404', [], 404);
    }
}
