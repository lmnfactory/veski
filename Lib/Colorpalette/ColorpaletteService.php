<?php

namespace Lmn\App\Veski\Lib\Colorpalette;

use Lmn\App\Veski\Repository\ColorpaletteRepository;

class ColorpaletteService {

    private $colorpaletteRepo;

    public function __construct(ColorpaletteRepository $colorpaletteRepo) {
        $this->colorpaletteRepo = $colorpaletteRepo;
    }

    public function getRandomColor($notIn = []) {
         $this->colorpaletteRepo->clear()
            ->criteria('core.rand');

        if (!empty($notIn)) {
            $this->colorpaletteRepo->criteria('core.notin', ['name' => 'id', 'value' => $notIn]);
        }

        return $this->colorpaletteRepo->get();
    }

    public function all() {
        return $this->colorpaletteRepo->clear()
            ->all();
    }
}
