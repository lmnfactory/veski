<?php

namespace Lmn\App\Veski\Lib\Cache;

use Lmn\Core\Lib\Cache\Cacheable;

use Lmn\App\Veski\Database\Model\Colorpalette;

class ColorpaletteCache implements Cacheable {

    public function cache() {
        return Colorpalette::get();
    }
}
